﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AttsWindow
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<string> names = new List<string>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Window1 win = new Window1();
            win.Owner = this;
            //DataContext = win.DataContext;

           /* foreach(field f in win.fields)
            {
                names.Add(f.Name);
            }*/

            listBox.ItemsSource = win.fieldList;
            win.Show();
            win.Closed += new EventHandler(win1_closed);
            

            //this.Close();
        }


        private void win1_closed(object sender, System.EventArgs e)
        {
            //listBox.ItemsSource = ((Window1)sender).fieldList;
            listBox.Items.Refresh();
        }
    }
}
